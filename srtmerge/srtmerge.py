import os.path as path
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
import srt
import re


cleanhtml = re.compile('<.*?>')


def rgb2hex(color):
    return "#" + \
        f"{int(round(255 * color.red, 0)):02X}" + \
        f"{int(round(255 * color.green, 0)):02X}" + \
        f"{int(round(255 * color.blue, 0)):02X}"


class MyWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Сшиватель субтитров")

        self.grid = Gtk.Grid()
        self.grid.set_column_spacing(2)
        self.grid.set_row_spacing(2)
        self.add(self.grid)

        self.filter_srt = Gtk.FileFilter()
        self.filter_srt.set_name("Subtitle files")
        self.filter_srt.add_mime_type("application/x-subrip")

        self.srt1 = Gtk.FileChooserButton.new(title="Первый файл",
                                              action=Gtk.FileChooserAction.OPEN)
        self.srt1.add_filter(self.filter_srt)
        self.grid.attach(self.srt1, 0, 0, 1, 1)

        self.clr1 = Gtk.ColorButton.new()
        self.clr1.set_rgba(Gdk.RGBA(1, 0, 0))
        self.grid.attach(self.clr1, 1, 0, 1, 1)

        self.bold1 = Gtk.CheckButton.new_with_label("Жирный")
        self.grid.attach(self.bold1, 2, 0, 1, 1)

        self.italic1 = Gtk.CheckButton.new_with_label("Курсив")
        self.grid.attach(self.italic1, 3, 0, 1, 1)

        self.ul1 = Gtk.CheckButton.new_with_label("Подчёркнутый")
        self.grid.attach(self.ul1, 4, 0, 1, 1)

        self.srt2 = Gtk.FileChooserButton.new(title="Второй файл",
                                              action=Gtk.FileChooserAction.OPEN)
        self.srt2.add_filter(self.filter_srt)
        self.grid.attach(self.srt2, 0, 1, 1, 1)

        self.clr2 = Gtk.ColorButton.new()
        self.clr2.set_rgba(Gdk.RGBA(0, 0, 1))
        self.grid.attach(self.clr2, 1, 1, 1, 1)

        self.bold2 = Gtk.CheckButton.new_with_label("Жирный")
        self.grid.attach(self.bold2, 2, 1, 1, 1)

        self.italic2 = Gtk.CheckButton.new_with_label("Курсив")
        self.grid.attach(self.italic2, 3, 1, 1, 1)

        self.ul2 = Gtk.CheckButton.new_with_label("Подчёркнутый")
        self.grid.attach(self.ul2, 4, 1, 1, 1)

        self.do = Gtk.Button(label="Сшить")
        self.grid.attach(self.do, 0, 2, 5, 1)

        self.do.connect("clicked", self.merge)

    def merge(self, widget):
        if self.srt1.get_filename() is None or \
           not path.isfile(self.srt1.get_filename()):
            self.show_error("Ошибка первого файла", "Файл не существует")
            return
        if self.srt2.get_filename() is None or \
           not path.isfile(self.srt2.get_filename()):
            self.show_error("Ошибка второго файла", "Файл не существует")
            return
        dialog = Gtk.FileChooserDialog(parent=self,
                                       action=Gtk.FileChooserAction.SAVE,
                                       create_folders=True,
                                       do_overwrite_confirmation=True)
        dialog.add_buttons(Gtk.STOCK_CANCEL,
                           Gtk.ResponseType.CANCEL,
                           Gtk.STOCK_OPEN,
                           Gtk.ResponseType.OK)
        dialog.add_filter(self.filter_srt)
        response = dialog.run()
        destination = dialog.get_filename()
        dialog.destroy()
        if response == Gtk.ResponseType.CANCEL:
            return
        s1 = None
        s2 = None
        try:
            with open(self.srt1.get_filename(), "r") as f:
                s1 = srt.parse(f.read())
        except Exception as E:
            self.show_error("Ошибка чтения первого файла", E)
        try:
            with open(self.srt2.get_filename(), "r") as f:
                s2 = srt.parse(f.read())
        except Exception as E:
            self.show_error("Ошибка чтения второго файла", E)
        try:
            with open(destination, "w") as f:
                for i, j in zip(s1, s2):
                    s1 = re.sub(cleanhtml, '', i.content.strip())
                    s2 = re.sub(cleanhtml, '', j.content.strip())
                    if self.bold1.get_active():
                        s1 = "<b>" + s1 + "</b>"
                    if self.bold2.get_active():
                        s2 = "<b>" + s2 + "</b>"
                    if self.italic1.get_active():
                        s1 = "<i>" + s1 + "</i>"
                    if self.italic2.get_active():
                        s2 = "<i>" + s2 + "</i>"
                    if self.ul1.get_active():
                        s1 = "<u>" + s1 + "</u>"
                    if self.ul2.get_active():
                        s2 = "<u>" + s2 + "</u>"
                    i.content = \
                        f'<font color="{rgb2hex(self.clr1.get_rgba())}">' + \
                        s1 + "</font>" + '\n' + \
                        f'<font color="{rgb2hex(self.clr2.get_rgba())}">' + \
                        s2 + "</font>"
                    f.write(srt.compose([i]))

        except Exception as E:
            self.show_error("Ошибка записи файла", E)

    def show_error(self, main, secondary):
        dialog = Gtk.MessageDialog(parent=self, flags=0,
                                   message_type=Gtk.MessageType.ERROR,
                                   buttons=Gtk.ButtonsType.CLOSE,
                                   text=main,
                                   secondary_text=secondary)
        dialog.run()
        dialog.destroy()


def main():
    win = MyWindow()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()
